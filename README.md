02807 - Computational tools for data science

Week 1

Excercise 1
Input:
awk '{print $1}' access.log-sample  | sort | uniq | wc -l 

Output:
147

Excercise 2:
a) amount of columns
input: 
head -n 1 titanic.csv | sed 's/,/ /g' | wc -w

output:
12

b) amount survived
input:
csvcut titanic.csv -c 2 | sort | uniq -c

Output:
    549 FALSE
      1 Survived
    342 TRUE

c) random 100 ratio

input: 
csvcut titanic.csv -c 2 | shuf -n 100 | sort | uniq -c

Output:
     64 FALSE
     36 TRUE

